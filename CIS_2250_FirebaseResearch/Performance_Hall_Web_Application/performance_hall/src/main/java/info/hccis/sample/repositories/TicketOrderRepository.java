package info.hccis.sample.repositories;

import info.hccis.sample.jpa.entity.TicketOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TicketOrderRepository extends CrudRepository<TicketOrder, Integer> {
}