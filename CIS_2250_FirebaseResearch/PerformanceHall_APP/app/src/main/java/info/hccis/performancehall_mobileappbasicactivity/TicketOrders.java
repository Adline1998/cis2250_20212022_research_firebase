package info.hccis.performancehall_mobileappbasicactivity;

public class TicketOrders {

    private String hollpassNumber;
    private String numberOfTickets;

        public TicketOrders() {

        }

        public TicketOrders(String hollpassNumber, String numberOfTickets) {
            this.hollpassNumber = hollpassNumber;
            this.numberOfTickets = numberOfTickets;
        }

    public void setHollpassNumber(String hollpassNumber) {
        this.hollpassNumber = hollpassNumber;
    }

    public String getNumberOfTickets() {
        return numberOfTickets;
    }

    public void setNumberOfTickets(String numberOfTickets) {
        this.numberOfTickets = numberOfTickets;
    }
    public String getHollpassNumber() {
        return hollpassNumber;
    }


}
