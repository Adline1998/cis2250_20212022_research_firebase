package info.hccis.performancehall_mobileappbasicactivity.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import info.hccis.performancehall_mobileappbasicactivity.entity.TicketOrder;

@Dao
public interface TicketOrderDAO {

    @Insert
    void insert(TicketOrder ticketOrder);

    @Query("SELECT * FROM TicketOrder")
    List<TicketOrder> selectAllTicketOrders();

    @Query("delete from TicketOrder")
    public void deleteAll();

}
